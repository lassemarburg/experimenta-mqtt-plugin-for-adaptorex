const schema = require('./schema.json')

/** @typedef {import('../../../plugins/plugin').PluginSetup} PluginSetup */
/** @typedef {import('../../../plugins/mqtt/mqtt').Plugin} MQTTPlugin */
/** @typedef {import('../../../types').Game} Game */
/** @typedef {import('../../../types').SessionInterface} Session */
/** @typedef {import('../../../types').AdaptorAction} Action */

class expMQTT extends adaptor.Plugin {
    constructor() {
        super(schema)
        this.core = false
        this.autoconnect = true
        this.client
        this.item_subscriptions = []
    }

    /** @type {PluginSetup} */
    async setup(config, game) {
        super.setup(config, game)

        /** @type {MQTTPlugin} */
        this.mqtt = game.getPlugin("mqtt")
        
        if(!this.mqtt) {
            game.status.on("plugins_loaded", e => {
                this.mqtt = game.getPlugin("mqtt")
            }) 
        }

        return this.schema
    }

    /**
     * Publish an MQTT Message including the current, updated status
     * @type {Action}
     */
    async sendExpStatusUpdate(data, session) {
        let ref = await session.variables.getVariableReference(data.to)
        let item_docs = await this.game.topics[ref.topic].getMany(ref.query)
        if(!item_docs.length) {
            throw new adaptor.NotFoundError(`Could not find mqtt subscription item ${data.to} in ${ref.topic}`)
        }
        let item = item_docs[0]
        data.message = await session.variables.review(data.message)
        data.message = session.variables.parseJSON(data.message)
        let message = {}
        
        if(typeof item.topic !== "string") {
            throw new adaptor.InvalidError(`Item ${data.to} is missing 'topic' property`)
        }

        if(data.topic) {
            if(item.status) {
                message = adaptor.getPath(item.status, data.topic.replace('/','.'))
            }
            data.topic = `${item.topic}/${data.topic}`
        } else {
            if(item.status) {
                message = item.status
            }
            data.topic = item.topic
        }
        
        if(ref.field) {
            adaptor.assignPath(message, ref.field, data.message)
        } else if(typeof data.message === "string") {
            message = data.message
        } else if(typeof message === "object") {
            Object.assign(message, data.message)
        } else  {
            message = data.message
        }
        data.message = message
        this.log.info(data)
        await this.mqtt.sendMQTTMessage(data, session)
    }

    /**
     * Publish an MQTT Message with audio file path as message
     * @type {Action}
     */
    async playExpAudio(data, session) {
        data.message = {playAudio:data.file}
        await this.mqtt.sendMQTTMessage(data, session)
    }
}

module.exports.Plugin = expMQTT